package com.syneos.cdh

import java.time.LocalDate

import com.syneos.custom.CdhCommon

object CdhTabOneSparkMain extends CdhSparkConvPrd {
  def main(args: Array[String]): Unit ={
    val currentDate = LocalDate.now.toString
    try {
      if(args(0) != "") {
        val date_value = args(0)
        //populating inactive data
        //cdhInactive(date_value)
  } else {
    throw new IllegalStateException("Arguments are missing. Please specify first argument as yesterday date")
  }

  } catch {
    case e: Exception =>
      val cdhInst: CdhCommon = new CdhCommon()
      cdhInst.sendMessageToSlack("Something Went Wrong in your Cdh Raw Spark Code "+currentDate+"")

  }
  }

}
