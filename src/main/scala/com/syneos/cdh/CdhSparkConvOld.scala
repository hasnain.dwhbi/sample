package com.syneos.cdh

import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types._
class CdhSparkConvOld {
  val spark = SparkSession
    .builder()
    .appName("Cdh Raw Hive to Spark Conversion")
    .master("yarn")
    .config("hive.metastore.uris","thrift://698772-rsus-hdmatst01.incresearch.com:9083")
    //.config("hive.exec.dynamic.partition.mode","nonstrict")
    //.config("spark.driver.extraClassPath", "/usr/hdp/2.6.1.0-129/sqoop/lib/sqljdbc42.jar")
    .config("spark.debug.maxToStringFields","100")
    .enableHiveSupport()
    .getOrCreate()
  import spark.implicits._
  val edcRawDatabaseName = "edc_rave_raw"
  val edcViewsDatabaseName = "edc_rave_views"
  val edcAggDatabaseName = "edc_rave_aggregation"
  val warehouseLocation  = "/apps/hive/warehouse"
  val rankInactiveWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid").orderBy($"source_id".cast(IntegerType) desc)
  val rankQueryAuditWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key").orderBy($"source_id".cast(IntegerType) desc)
  val rankInactiveCrfsWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id").orderBy($"source_id".cast(IntegerType) desc)
  def cdhInactive(){
    val ravedataDF = spark.sql("select * from "+edcRawDatabaseName+".rave_data_events_incremental")
    val raveFilteredDF = ravedataDF.filter($"audit_sub_category_name".isin ("MigDPDeleted","Deleted", "Inactivated", "InactivatedWithReasonCode","Activated","ActivatedWithReasonCode"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","audit_sub_category_name","audit_date_time_stamp","source_id")
      .withColumn("rank",rank() over rankInactiveWindow)
    val raveFinalDF = raveFilteredDF.filter($"rank" === "1" and $"audit_sub_category_name".isin("Inactivated", "InactivatedWithReasonCode","MigDPDeleted","Deleted"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","audit_sub_category_name","audit_date_time_stamp","source_id")
    //raveFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/inactiveinvisible_datapoints")

    val raveFilteredDFU = ravedataDF.filter($"audit_sub_category_name".isin ("SetInvisible","SetVisible")).select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","audit_sub_category_name","audit_date_time_stamp","source_id")
      .withColumn("rank",rank() over rankInactiveWindow)
    val raveFinalDFU = raveFilteredDFU.filter($"rank" === "1" and $"audit_sub_category_name".isin("SetInvisible"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","audit_sub_category_name","audit_date_time_stamp","source_id")
    //raveFinalDF.write.mode("append").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/inactiveinvisible_datapoints")
    val raveFinalUnion = raveFinalDF.union(raveFinalDFU)

    //populating active data points
    cdhActiveDataPoints(ravedataDF,raveFinalUnion)
    //populating inactive crfs
    cdhInactiveCrfs(ravedataDF)
  }

  //code populating active data points
  def cdhActiveDataPoints(raveDataFrame: DataFrame,inactiveDataFrame: DataFrame){
    val inactiveDF = inactiveDataFrame.withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid"))
    val raveDataDF = raveDataFrame.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_sub_category_name","audit_date_time_stamp","source_id","audit_user_oid","query_recipient","item_value","meta_data_version_oid","subject_status","query_value","subject_key")
      .withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid"))
      .filter($"project_id" =!= "1009920B" )

    // code for active data points table population
    val activeDataPointsDF = raveDataDF.join(inactiveDF, inactiveDF("key1") === raveDataDF("key1"),"left").filter(inactiveDF("key1").isNull)
      .select(raveDataDF("project_id"),raveDataDF("study_id"),raveDataDF("site_ref_id"),raveDataDF("subject_name"),raveDataDF("study_event_oid"),raveDataDF("study_event_repeat_key"),raveDataDF("form_oid"),raveDataDF("form_repeat_key"),raveDataDF("record_id"),raveDataDF("item_oid"),raveDataDF("item_value"),raveDataDF("meta_data_version_oid"),raveDataDF("query_repeat_key"),raveDataDF("audit_sub_category_name"),raveDataDF("audit_date_time_stamp"),raveDataDF("source_id"),raveDataDF("audit_user_oid"),raveDataDF("query_recipient"),raveDataDF("subject_status"),raveDataDF("query_value"),raveDataDF("subject_key"),raveDataDF("key1"))
    activeDataPointsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcViewsDatabaseName+".db/activedatapoints")

    // code for active data points query table population
    val activeDataPointsQueryDF = activeDataPointsDF.filter($"audit_sub_category_name".isin ("QueryOpen","QueryClose","QueryAnswerByChange","QueryAnswer","MigQueryClosed","QueryCancel"))
    activeDataPointsQueryDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/activedatapoints_query")

    // code for populating query audit actions
    cdhQueryAuditActions(activeDataPointsDF)
  }

  // code for populating query audit actions
  def cdhQueryAuditActions(activeDataPoints: DataFrame){
      val queryAuditActionsDF = activeDataPoints.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_sub_category_name","audit_date_time_stamp","source_id")
      .filter($"audit_sub_category_name".isin("QueryOpen","QueryClose","QueryAnswerByChange","QueryAnswer","MigQueryClosed","QueryCancel") and $"subject_name" =!= "New Subject")
    queryAuditActionsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/queryauditactions")
  }

  //populating inactive crfs
  def cdhInactiveCrfs(raveDataFrame: DataFrame){
    val inactiveCrfsDF = raveDataFrame.filter($"audit_sub_category_name".isin("Inactivated","InactivatedWithReasonCode","Activated","ActivatedWithReasonCode") and $"item_oid" === "" and $"record_id" === "" and $"form_oid" =!= "")
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id","audit_sub_category_name")
    .withColumn("rank",rank() over rankInactiveCrfsWindow)
    val inactiveCrfsFinalDF = inactiveCrfsDF.filter($"rank" === 1 and $"audit_sub_category_name".isin("Inactivated","InactivatedWithReasonCode"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id")
    inactiveCrfsFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/inactive_crfs")

    //populating CrfFormStatusDetail
    cdhCrfFormStatusDetail(raveDataFrame,inactiveCrfsFinalDF)
  }

  def cdhCrfFormStatusDetail(raveDataFrame: DataFrame, inactiveCrfsDataFrame: DataFrame){
    val raveDF = raveDataFrame.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id","audit_sub_category_name")
      .withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"data_page_id"))

    val inactiveCrfsDF = inactiveCrfsDataFrame.withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"data_page_id"))

    val crfFormDF = raveDF.join(inactiveCrfsDF, inactiveCrfsDF("key1") === raveDF("key1"),"left").filter(raveDF("key1").isNull).select(raveDF("project_id"),raveDF("site_ref_id"),raveDF("subject_name"),raveDF("study_event_oid"),raveDF("study_event_repeat_key"),raveDF("form_oid"),raveDF("form_repeat_key"),raveDF("data_page_id"),raveDF("audit_sub_category_name"),raveDF("key1"))
    val crfFormFinalDF = crfFormDF.filter(!$"form_oid".rlike("CTMS|INTEG") and !$"subject_name" === "New Subject")
    crfFormFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crfform_status_details")
  }

}
