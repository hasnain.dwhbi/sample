package com.syneos.cdh

import java.time.LocalDate

import com.syneos.custom.CdhCommon
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

object CdhTabTwoSparkMainPrd extends CdhSparkConvPrd {
  def main(args: Array[String]): Unit ={
    val cdhInst: CdhCommon = new CdhCommon()
    val currentDate = LocalDate.now.toString
    try {
      /*val f = Future {
        populateCrfStartedSubjects()
        populateCrfEntryComplete()
        "success"
      }
        f.onComplete {
          case Success(value) => {
            if(value == "Success"){
              cdhInst.sendMessageToSlack("Parallel execution is done for Cdh Tab Two Spark Code On "+currentDate+" ."+value)
            }
          }
          case Failure(e) => {
            cdhInst.sendMessageToSlack("Something Went Wrong in your Cdh Tab Two Spark Code On "+currentDate+". Error Message: "+e.getStackTrace)
          }
        }
      */
        cacheActiveDataPoints()
        populateExpectedCrfs()
        populateCrfExpectedSubjects()
        populateCrfStartedSubjects()
        populateCrfEntryComplete()
        populateCrfEntryCompleteBase()
        populateCrfClosedQueriesSubj()
        populateCrfSdvCompleteSubjTwo()
        populateCrfReadySignBase()
        populateCrfReadySignSubjects()
        populateCrfPiSignedSubj()
        populateActiveEnteredReqSdvDpTabTwo()
        populateCrfReqSdvSubj()
        populateAuditQueriesTabTwo()
        populateAuditCrfsTabTwo()
        populateAuditQueriesCrfsTermsTabTwo()
        populatePdCDHTabTwo()
  } catch {
    case e: Exception =>
      cdhInst.sendMessageToSlack("Something Went Wrong in your Cdh Tab Two Spark Code On "+currentDate)
  }
  }
}
