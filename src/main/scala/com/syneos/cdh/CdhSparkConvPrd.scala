package com.syneos.cdh

import com.syneos.custom.CdhCommon
import org.apache.spark.sql._
import org.apache.spark.sql.functions._
import org.apache.spark.sql.expressions.Window
import org.apache.spark.sql.types._

class CdhSparkConvPrd extends CdhCommon{
  val spark = SparkSession
    .builder()
    .appName("Cdh Hive to Spark Conversion")
    .master("yarn")
    //.config("hive.metastore.uris","thrift://698772-rsus-hdmatst01.incresearch.com:9083") // this is for test
    .config("hive.metastore.uris","thrift://698763-rsus-hdmaprd01.incresearch.com:9083") //this is for prod
    .config("spark.debug.maxToStringFields","100")
    .config("spark.sql.shuffle.partitions", "10")
    .enableHiveSupport()
    .getOrCreate()
  import spark.implicits._
  val edcRawDatabaseName = "edc_rave_raw"
  val edcViewsDatabaseName = "edc_rave_views"
  val edcAggDatabaseName = "edc_rave_aggregation"
  val warehouseLocation  = "/apps/hive/warehouse"
  val eedcRaveDatabaseName = "eedc_rave"
  val sbiPresDatabaseName = "sbi_presentation"
  val inactiveInvisibleTableNameV1 = "inactiveinvisible_datapoints_v1"
  val inactiveInvisibleTableNameV2 = "inactiveinvisible_datapoints_v2"
  val activeDataPointsV1 = "InactiveInvisible_DataPoints_v1"
  val activeDataPointsV2 = "InactiveInvisible_DataPoints_v2"
  val raveRawDataDirectory = "/user/nifi/inc/rave/data/event_table_v1"
  val raveExternalLocation = "/user/nifi/inc/rave/data"
  val alsDataLocation = "/user/eedc/als/als_csv"

  val rankInactiveWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid").orderBy($"source_id".cast(IntegerType) desc)
  val rankQueryAuditWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key").orderBy($"source_id".cast(IntegerType) desc)
  val rankInactiveCrfsWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id").orderBy($"source_id".cast(IntegerType) desc)

  val rvalsAdpWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid").orderBy($"source_id".cast(IntegerType) desc)


  // defination to get the dataframe for raw data and get empty data if event ids not exists
  def getDataFrame(url: String,schema: StructType,default_url: String = "/lrm-adheris/dummy"): DataFrame ={
    try {
      spark.read.format("orc").schema(schema).load(url)
    } catch {
      case _: Throwable => {}
        spark.read.format("orc").schema(schema).load(default_url)
    }
  }

  /* start code for rave cdh raw workflow */
  //code populating active data points
  def cdhActiveDataPoints(raveDataFrame: DataFrame,inactiveDataFrame: DataFrame){
    val inactiveDF = inactiveDataFrame.withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid"))
    val raveDataDF = raveDataFrame.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_sub_category_name","audit_date_time_stamp","source_id","audit_user_oid","query_recipient","item_value","meta_data_version_oid","subject_status","query_value","subject_key","data_page_id")
      .withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid"))
      .filter($"project_id" =!= "1009920B" )

    // code for active data points table population
    val activeDataPointsDF = raveDataDF.join(inactiveDF, inactiveDF("key1") === raveDataDF("key1"),"left").filter(inactiveDF("key1").isNull)
      .select(raveDataDF("project_id"),raveDataDF("study_id"),raveDataDF("site_ref_id"),raveDataDF("subject_name"),raveDataDF("study_event_oid"),raveDataDF("study_event_repeat_key"),raveDataDF("form_oid"),raveDataDF("form_repeat_key"),raveDataDF("record_id"),raveDataDF("item_oid"),raveDataDF("item_value"),raveDataDF("meta_data_version_oid"),raveDataDF("query_repeat_key"),raveDataDF("audit_sub_category_name"),raveDataDF("audit_date_time_stamp"),raveDataDF("source_id"),raveDataDF("audit_user_oid"),raveDataDF("query_recipient"),raveDataDF("subject_status"),raveDataDF("query_value"),raveDataDF("subject_key"),raveDataDF("key1"))
    activeDataPointsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcViewsDatabaseName+".db/activedatapoints")

    // code for active data points query table population
    val activeDataPointsQueryDF = activeDataPointsDF.filter($"audit_sub_category_name".isin ("QueryOpen","QueryClose","QueryAnswerByChange","QueryAnswer","MigQueryClosed","QueryCancel"))
    activeDataPointsQueryDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/activedatapoints_query")

    // code for populating query audit actions
    cdhQueryAuditActions(activeDataPointsDF)
  }

  // code for populating query audit actions
  def cdhQueryAuditActions(activeDataPoints: DataFrame){
    val queryAuditActionsDF = activeDataPoints.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_sub_category_name","audit_date_time_stamp","source_id")
      .filter($"audit_sub_category_name".isin("QueryOpen","QueryClose","QueryAnswerByChange","QueryAnswer","MigQueryClosed","QueryCancel") and $"subject_name" =!= "New Subject")
    queryAuditActionsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/queryauditactions")
  }

  //populating inactive crfs
  def cdhInactiveCrfs(raveDataFrame: DataFrame){
    val inactiveCrfsDF = raveDataFrame.filter($"audit_sub_category_name".isin("Inactivated","InactivatedWithReasonCode","Activated","ActivatedWithReasonCode") and $"item_oid" === "" and $"record_id" === "" and $"form_oid" =!= "")
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id","audit_sub_category_name")
      .withColumn("rank",rank() over rankInactiveCrfsWindow)
    val inactiveCrfsFinalDF = inactiveCrfsDF.filter($"rank" === 1 and $"audit_sub_category_name".isin("Inactivated","InactivatedWithReasonCode"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id")
    inactiveCrfsFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/inactive_crfs")

    //populating CrfFormStatusDetail
    cdhCrfFormStatusDetail(raveDataFrame,inactiveCrfsFinalDF)
  }

  def cdhCrfFormStatusDetail(raveDataFrame: DataFrame, inactiveCrfsDataFrame: DataFrame){
    val raveDF = raveDataFrame.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","data_page_id","audit_sub_category_name")
      .withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"data_page_id"))

    val inactiveCrfsDF = inactiveCrfsDataFrame.withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"data_page_id"))

    val crfFormDF = raveDF.join(inactiveCrfsDF, inactiveCrfsDF("key1") === raveDF("key1"),"left").filter(raveDF("key1").isNull).select(raveDF("project_id"),raveDF("site_ref_id"),raveDF("subject_name"),raveDF("study_event_oid"),raveDF("study_event_repeat_key"),raveDF("form_oid"),raveDF("form_repeat_key"),raveDF("data_page_id"),raveDF("audit_sub_category_name"),raveDF("key1"))
    val crfFormFinalDF = crfFormDF.filter(!$"form_oid".rlike("CTMS|INTEG") and !$"subject_name" === "New Subject")
    crfFormFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crfform_status_details")
  }
  /* end code for rave cdh raw workflow */

  /*start of cdh Tab1 code*/

  def populateProjectStartEndDts(dt_value: String){
    val raveRawSchema = StructType(List(StructField("form_repeat_key",StringType,true),StructField("protocol_deviation_code",StringType,true),StructField("subject_status",StringType,true),StructField("signature_location_oid",StringType,true),StructField("item_data_transaction_type",StringType,true),StructField("item_value",StringType,true),StructField("protocol_deviation_repeat_key",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("form_oid",StringType,true),StructField("freeze",StringType,true),StructField("project_id",StringType,true),StructField("lock",StringType,true),StructField("comment_value",StringType,true),StructField("audit_location_oid",StringType,true),StructField("item_oid",StringType,true),StructField("study_oid",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("instance_id",StringType,true),StructField("connection_url",StringType,true),StructField("reason_for_change",StringType,true),StructField("signature_date_time_stamp",StringType,true),StructField("subject_name",StringType,true),StructField("item_group_repeat_key",StringType,true),StructField("study_event_oid",StringType,true),StructField("subject_data_transaction_type",StringType,true),StructField("protocol_deviation_status",StringType,true),StructField("subject_key_type",StringType,true),StructField("query_status",StringType,true),StructField("item_group_data_transaction_type",StringType,true),StructField("protocol_deviation_transaction_type",StringType,true),StructField("hadoop_row_id",StringType,true),StructField("query_response",StringType,true),StructField("form_data_transaction_type",StringType,true),StructField("site_ref_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("verify",StringType,true),StructField("data_page_id",StringType,true),StructField("subject_key",StringType,true),StructField("query_recipient",StringType,true),StructField("rave_write_timestamp",StringType,true),StructField("query_value",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("signature_user_oid",StringType,true),StructField("instance_name",StringType,true),StructField("protocol_deviation_class",StringType,true),StructField("study_data_transaction_type",StringType,true),StructField("comment_transaction_type",StringType,true),StructField("query_repeat_key",StringType,true),StructField("protocol_deviation_value",StringType,true),StructField("item_group_oid",StringType,true),StructField("record_id",StringType,true),StructField("rave_ws_timestamp",StringType,true),StructField("source_id",StringType,true),StructField("signature_oid",StringType,true),StructField("comment_repeat_key",StringType,true)))
    val raveDataUrl = getCdhRawDataUrl(raveRawDataDirectory,dt_value)
    val raveData = getDataFrame(raveDataUrl,raveRawSchema)
    val aggRaveCurrentDF = raveData.groupBy("project_id")
      .agg(to_date(min("audit_date_time_stamp")) as "start_date",to_date(max("audit_date_time_stamp")) as "end_date",to_date(min("audit_date_time_stamp")) as "process_date")
      .select("project_id","audit_date_time_stamp","start_date","end_date","process_date").withColumn("processed_timestamp",current_timestamp())
    //val aggRaveCurrentDF = aggRaveDF.select("project_id","start_date","end_date","process_date").withColumn("processed_timestamp",current_timestamp())

    //previous data
    val aggRaveDataSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("process_date",DateType,true),StructField("processed_timestamp",TimestampType,true)))
    val aggRaveAllData = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/projects_start_end_dates",aggRaveDataSchema)

    // Old records

    val aggRaveMinusTodayDF = aggRaveAllData.join(aggRaveCurrentDF,aggRaveAllData("project_id") === aggRaveCurrentDF("project_id"),"full_outer")
      .select(aggRaveAllData("project_id"),aggRaveAllData("start_date"),aggRaveAllData("end_date"),aggRaveAllData("process_date"),aggRaveAllData("processed_timestamp")
        ,aggRaveCurrentDF("project_id").as("project_id_c"),aggRaveCurrentDF("start_date").as("start_date_c"),aggRaveCurrentDF("end_date").as("end_date_c"),aggRaveCurrentDF("process_date").as("process_date_c"),aggRaveCurrentDF("processed_timestamp").as("processed_timestamp_c"))

    val aggMatchedDF = aggRaveMinusTodayDF.filter($"project_id".isNotNull and $"project_id_c".isNotNull).select($"project_id_c".as("project_id"),$"start_date_c".as("start_date"),$"end_date_c".as("end_date"),$"process_date_c".as("process_date"),$"processed_timestamp_c".as("processed_timestamp"))

    val aggNewDF = aggRaveMinusTodayDF.filter($"project_id".isNull).select($"project_id_c".as("project_id"),$"start_date_c".as("start_date"),$"end_date_c".as("end_date"),$"process_date_c".as("process_date"),$"processed_timestamp_c".as("processed_timestamp"))

    val aggOldDF = aggRaveMinusTodayDF.filter($"project_id_c".isNull).select($"project_id",$"start_date",$"end_date",$"process_date",$"processed_timestamp")

    val aggUnionDF = aggMatchedDF.union(aggNewDF).union(aggOldDF)
    aggUnionDF.cache()

    aggUnionDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/projects_start_end_dates")

  }

  def populateProjectSeDates(){
    val aggRaveDataSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("process_date",DateType,true),StructField("processed_timestamp",TimestampType,true)))
    val aggRaveAllData = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/projects_start_end_dates",aggRaveDataSchema)
    val seDatesDF = aggRaveAllData.select("project_id","start_date","end_date").distinct()
    seDatesDF.write.mode("append").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/project_se_dates")
  }

  def populateProjectWeekDays(){
    val d_date = spark.sql("select distinct iso_date as mydate from sbi_star.d_date")
    val prjSeDatesSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true)))
    val prjSeDates = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_se_dates",prjSeDatesSchema)
    val prjDatesDF = d_date.join(prjSeDates).filter(d_date("mydate").between(prjSeDates("start_date"),prjSeDates("end_date"))).orderBy(d_date("mydate") desc)
      .selectExpr("project_id","start_date","end_date","mydate","date_sub(to_date(mydate),pmod(datediff(to_date(mydate),'1900-01-05'),-7)) as friday_date")
      .select("project_id","start_date","end_date","friday_date").distinct()

    prjDatesDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates")
  }

  def populateQueriesLookup(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))

    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)

    val wkndFinalDF = wkndDF.select("project_id","friday_date","start_date").distinct()

    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsFinalDF = activeDataPointsDF.select($"project_id",$"site_ref_id",$"audit_date_time_stamp").distinct()

    val queriesLookupDF = wkndFinalDF.join(activeDataPointsDF)
      .where(wkndFinalDF("project_id") === activeDataPointsDF("project_id") and to_date(activeDataPointsDF("project_id")).between(wkndFinalDF("start_date"),current_date()))
      .select(wkndFinalDF("project_id"),activeDataPointsDF("site_ref_id"),wkndFinalDF("friday_date")).distinct()

    queriesLookupDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/queries_lookup")

  }

  def populateQueryAging(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))
    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)
    val wkndFinalDF = wkndDF.select("project_id","friday_date","start_date").distinct()

    val queryAuditActionsSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("rank",StringType,true)))
    val queryAuditActionDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/queryauditactions",queryAuditActionsSchema)

    val queryAuditActionFinalDF = queryAuditActionDF.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_date_time_stamp","audit_sub_category_name","rank")

    val wkndQueryAuditDF = wkndFinalDF.join(queryAuditActionFinalDF,"inner")
      .where(wkndFinalDF("project_id")===queryAuditActionFinalDF("project_id")
        and queryAuditActionFinalDF("audit_sub_category_name").isin("QueryOpen","QueryAnswer","QueryAnswerByChange")
        and queryAuditActionFinalDF("rank") === 1
        and to_date(queryAuditActionFinalDF("audit_date_time_stamp")).between(wkndFinalDF("start_date"),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),queryAuditActionFinalDF("site_ref_id"),wkndFinalDF("friday_date"),queryAuditActionFinalDF("subject_name"),queryAuditActionFinalDF("study_event_oid"),queryAuditActionFinalDF("study_event_repeat_key"),queryAuditActionFinalDF("form_oid"),queryAuditActionFinalDF("form_repeat_key"),queryAuditActionFinalDF("record_id"),queryAuditActionFinalDF("item_oid"),queryAuditActionFinalDF("query_repeat_key"),queryAuditActionFinalDF("audit_date_time_stamp"),queryAuditActionFinalDF("audit_sub_category_name"))
    wkndQueryAuditDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging")

  }

  def populateTotalOpenQueriesCurrentAndAllWeek(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))
    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)
    val wkndFinalDF = wkndDF.select("project_id","friday_date","start_date")

    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsFinalDF = activeDataPointsDF.select($"project_id",$"site_ref_id",$"subject_name",$"query_repeat_key",to_date($"audit_date_time_stamp").as("open_date"),$"audit_sub_category_name")

    //logic for current week
    val wkndAdpCurrDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryOpen")
        and activeDataPointsFinalDF("open_date").between(date_add(wkndFinalDF("friday_date"),-7),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpCurrDF = wkndAdpCurrDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "Open_Queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","Open_Queries")
    aggWkndAdpCurrDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_open_queries_current_week")

    // logic for all weeks
    val wkndAdpAllDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryOpen")
        and activeDataPointsFinalDF("open_date").between(wkndFinalDF("start_date"),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpAllDF = wkndAdpAllDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "Open_Queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","Open_Queries")

    aggWkndAdpAllDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_open_queries_week")

  }


  def populateTotalAnswerQueriesCurrentAndAllWeek(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))
    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)
    val wkndFinalDF = wkndDF.select("project_id","friday_date","start_date")

    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsFinalDF = activeDataPointsDF.select($"project_id",$"site_ref_id",$"subject_name",$"query_repeat_key",to_date($"audit_date_time_stamp").as("ans_date"),$"audit_sub_category_name")

    // code for current week
    val wkndAdpCurrDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryAnswer","QueryAnswerByChange")
        and activeDataPointsFinalDF("ans_date").between(date_add(wkndFinalDF("friday_date"),-7),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpCurrDF = wkndAdpCurrDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "answer_queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","answer_queries")

    aggWkndAdpCurrDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_answer_queries_current_week")

    // code for all weeks
    val wkndAdpAllDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryAnswer","QueryAnswerByChange")
        and activeDataPointsFinalDF("ans_date").between(wkndFinalDF("start_date"),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpAllDF = wkndAdpAllDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "answer_queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","answer_queries")

    aggWkndAdpAllDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_ans_queries_week")



  }

  def populateTotalCloseQueriesWeek(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))
    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)
    val wkndFinalDF = wkndDF.select("project_id","friday_date","start_date")

    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsFinalDF = activeDataPointsDF.select($"project_id",$"site_ref_id",$"subject_name",$"query_repeat_key",to_date($"audit_date_time_stamp").as("close_date"),$"audit_sub_category_name")

    val wkndAdpDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryClose")
        and activeDataPointsFinalDF("close_date").between(wkndFinalDF("start_date"),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpDF = wkndAdpDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "close_queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","close_queries")

    aggWkndAdpDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_close_queries_week")

  }


  def populateTotalCancelledQueriesCurrentAndAllWeek(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))
    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)
    val wkndFinalDF = wkndDF.select("project_id","friday_date","start_date")

    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsFinalDF = activeDataPointsDF.select($"project_id",$"site_ref_id",$"subject_name",$"query_repeat_key",to_date($"audit_date_time_stamp").as("cancel_date"),$"audit_sub_category_name")

    //code for all week
    val wkndAdpAllDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryCancel","MigQueryClosed")
        and activeDataPointsFinalDF("cancel_date").between(wkndFinalDF("start_date"),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpAllDF = wkndAdpAllDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "cancel_queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","cancel_queries")

    aggWkndAdpAllDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_ever_cancelled_queries_week")

    // code for current week
    val wkndAdpCurrDF = wkndFinalDF.join(activeDataPointsFinalDF,"inner")
      .where(wkndFinalDF("project_id")===activeDataPointsFinalDF("project_id")
        and activeDataPointsFinalDF("audit_sub_category_name").isin("QueryCancel","MigQueryClosed")
        and activeDataPointsFinalDF("cancel_date").between(date_add(wkndFinalDF("friday_date"),-7),wkndFinalDF("friday_date"))
      ).select(wkndFinalDF("project_id"),activeDataPointsFinalDF("site_ref_id"),wkndFinalDF("friday_date"))

    val aggWkndAdpCurrDF = wkndAdpCurrDF
      .groupBy("project_id","friday_date","site_ref_id")
      .agg(count(lit("*")) as "cancel_queries").orderBy("friday_date desc")
      .select("project_id","friday_date","site_ref_id","cancel_queries")

    aggWkndAdpCurrDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/total_cancelled_queries_current_week")

  }



  def populateQueryAgingAll(){
    val queryAgingSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("friday_date",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("audit_sub_category_name",StringType,true)))
    val queryAgingDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging",queryAgingSchema)
    val queryAgingFinalDF = queryAgingDF.filter($"audit_sub_category_name".isin("QueryOpen"))
      .select($"project_id",$"site_ref_id",$"friday_date",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid",$"query_repeat_key",datediff($"friday_date",to_date($"audit_date_time_stamp")).as("dt"))

    // code for query aging 7 days
    val AggQueryAging7DF = queryAgingFinalDF
        .groupBy("project_id","site_ref_id","friday_date")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key") as "Query_Aging_7")
      .filter(queryAgingFinalDF("dt").between(0,7))
      .select("project_id","site_ref_id","friday_date","Query_Aging_7")

    AggQueryAging7DF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging_7")


    // code for query aging 14 days
    val AggQueryAging14DF = queryAgingFinalDF
      .groupBy("project_id","site_ref_id","friday_date")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key") as "Query_Aging_14")
      .filter(queryAgingFinalDF("dt").between(8,14))
      .select("project_id","site_ref_id","friday_date","Query_Aging_14")

    AggQueryAging14DF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging_14")


    // code for query aging 29 days
    val AggQueryAging29DF = queryAgingFinalDF
      .groupBy("project_id","site_ref_id","friday_date")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key") as "Query_Aging_29")
      .filter(queryAgingFinalDF("dt").between(15,29))
      .select("project_id","site_ref_id","friday_date","Query_Aging_29")

    AggQueryAging29DF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging_29")

    // code for query aging gt 29 days
    val AggQueryAgingGt29DF = queryAgingFinalDF
      .groupBy("project_id","site_ref_id","friday_date")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key") as "Query_Aging_gt_29")
      .filter(queryAgingFinalDF("dt").gt(29))
      .select("project_id","site_ref_id","friday_date","Query_Aging_gt_29")

    AggQueryAgingGt29DF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging_gt_29")

    // code for query aging 30 days
    val AggQueryAging30DF = queryAgingFinalDF
      .groupBy("project_id")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key") as "Query_Aging_gt_30")
      .filter(queryAgingFinalDF("dt").gt(30))
      .select("project_id","Query_Aging_gt_30")

    AggQueryAging30DF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/query_aging_gt_30")


  }

  // code related to All_Time_To_Data_Entry
  def populateRvalsVisitdates() {
    val raveConnectionSchema = StructType(List(StructField("id", StringType, true),StructField("project_id", StringType, true),StructField("project_oid", StringType, true),StructField("project_name", StringType, true),StructField("sponsor_name", StringType, true),StructField("connection_url", StringType, true),StructField("auth_id", StringType, true),StructField("auth_pin", StringType, true),StructField("auth_passcode", StringType, true),StructField("created_ts", StringType, true),StructField("status", StringType, true),StructField("comments", StringType, true),StructField("created_by", StringType, true),StructField("edit_reason", StringType, true),StructField("max_connections", StringType, true)))
    val rvalsFieldsProdSchema = StructType(List(StructField("fields_draftfieldname",StringType,true),StructField("fields_length",StringType,true),StructField("fields_variableoid",StringType,true),StructField("metadata_version_oid",StringType,true),StructField("project_name",StringType,true),StructField("fields_entryrestrictions",StringType,true),StructField("fields_fixedunit",StringType,true),StructField("fields_indentlevel",StringType,true),StructField("fields_ordinal",StringType,true),StructField("fields_visible",StringType,true),StructField("fields_datadictionaryname",StringType,true),StructField("project_oid",StringType,true),StructField("fields_datatype",StringType,true),StructField("fields_sourcedocument",StringType,true),StructField("fields_viewrestrictions",StringType,true),StructField("fields_draftfieldactive",StringType,true),StructField("fields_doesnotbreaksignature",StringType,true),StructField("created",StringType,true),StructField("primary_form_oid",StringType,true),StructField("fields_cansetstudyeventdate",StringType,true),StructField("fields_codingmodetype",StringType,true),StructField("fields_labelpositions",StringType,true),StructField("fields_labels",StringType,true),StructField("fields_dateformat",StringType,true),StructField("fields_reviewgroup",StringType,true),StructField("fields_saslabel",StringType,true),StructField("fields_oid",StringType,true),StructField("fields_controltype",StringType,true),StructField("fields_pretext",StringType,true),StructField("medidata_version_name",StringType,true),StructField("study_id",StringType,true)))
    val rvalsFieldsProdDF = getDataFrame(raveExternalLocation+"/rvals_fields_prod",rvalsFieldsProdSchema)
    val raveConnectionDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/rave_connection",raveConnectionSchema)
    val rvalsRaveFieldsDF = rvalsFieldsProdDF.filter(trim($"fields_cansetstudyeventdate") === "Yes")
      .join(raveConnectionDF,rvalsFieldsProdDF("project_oid") === raveConnectionDF("project_oid"),"left")
      .select(rvalsFieldsProdDF("project_oid"),raveConnectionDF("project_id"),rvalsFieldsProdDF("fields_oid"),rvalsFieldsProdDF("fields_draftfieldname"),rvalsFieldsProdDF("fields_pretext"))
      .distinct()

    rvalsRaveFieldsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/rvals_visit_dates")
  }

  def populateSubjectVisitDates(){
    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val rvalsVistDatesSchema = StructType(List(StructField("project_oid",StringType,true),StructField("project_id",StringType,true),StructField("fields_oid",StringType,true),StructField("fields_draftfieldname",StringType,true),StructField("fields_pretext",StringType,true)))
    val rvalsVisitDatesDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/rvals_visit_dates",rvalsVistDatesSchema)
    val activeDataPointsFilterDF = activeDataPointsDF.filter($"audit_sub_category_name".isin ("Entered","EnteredByDdeAutoReconcile","EnteredWithChangeCode") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))

    val adpRvlsDF = activeDataPointsFilterDF.join(rvalsVisitDatesDF,rvalsVisitDatesDF("project_id") === activeDataPointsFilterDF("project_id") and rvalsVisitDatesDF("fields_oid") === activeDataPointsFilterDF("item_oid") ,"left")
      .select(activeDataPointsFilterDF("project_id"),activeDataPointsFilterDF("site_ref_id"),activeDataPointsFilterDF("subject_name"),activeDataPointsFilterDF("study_event_oid"),activeDataPointsFilterDF("study_event_repeat_key"),activeDataPointsFilterDF("form_oid"),activeDataPointsFilterDF("form_repeat_key"),activeDataPointsFilterDF("item_oid"),regexp_replace(activeDataPointsFilterDF("item_value")," ","/").as("item_value"),activeDataPointsFilterDF("audit_sub_category_name"),activeDataPointsFilterDF("audit_date_time_stamp"),activeDataPointsFilterDF("source_id"))

    val adpRvlsFDF = adpRvlsDF.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid","item_value","audit_sub_category_name","audit_date_time_stamp","source_id").withColumn("rank", rank() over rvalsAdpWindow)
      .filter($"rank" === 1)
      .select($"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"item_value".as("visit_date")).distinct()

    val activeDPFinalDF = adpRvlsFDF
      .groupBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key")
      .agg(min(to_date(from_unixtime(unix_timestamp($"visit_date","dd/MMM/yyyy")))) as "visit_date")
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","visit_date")
    activeDPFinalDF.coalesce(3).write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/subject_visit_date")
  }

  def populateVisitFirstForm(){
    val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsFilterDF = activeDataPointsDF.filter($"audit_sub_category_name".isin ("Entered","EnteredEmpty") and $"subject_name" =!= "New Subject" and !$"form_oid".like("CTMS%") and !nvlUDF($"form_oid",lit("0")).like("INTEG%") and !nvlUDF($"audit_user_oid",lit("0")).rlike("systemuser|RWS"))
    val visitFirstFormDF = activeDataPointsFilterDF.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","audit_date_time_stamp")
      .groupBy("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key")
      .agg(min($"audit_date_time_stamp") as "first_form_entered_date")

    visitFirstFormDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/visit_first_form")
  }

  def populateSubjectVisitTde(){
    val subjectVisitDateSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("visit_date",StringType,true)))
    val visitFirstFormSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("first_form_entered_date",StringType,true)))
    val windowValue = Window.partitionBy().orderBy("study_event_repeat_key desc")
    val visitFirstFormDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/visit_first_form",visitFirstFormSchema)
    val subjectVisitDateDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/subject_visit_date",subjectVisitDateSchema)

    val visitSubjectDataDF = visitFirstFormDF.join(subjectVisitDateDF,subjectVisitDateDF("project_id") === visitFirstFormDF("project_id") and subjectVisitDateDF("site_ref_id") === visitFirstFormDF("site_ref_id") and subjectVisitDateDF("subject_name") === visitFirstFormDF("subject_name") and subjectVisitDateDF("study_event_oid") === visitFirstFormDF("study_event_oid") and subjectVisitDateDF("study_event_repeat_key") === visitFirstFormDF("study_event_repeat_key"),"inner")
    .select(visitFirstFormDF("project_id"),visitFirstFormDF("site_ref_id"),visitFirstFormDF("subject_name"),visitFirstFormDF("study_event_oid"),visitFirstFormDF("study_event_repeat_key"),to_date(visitFirstFormDF("first_form_entered_date")).as("first_form_entered_date"),subjectVisitDateDF("visit_date"))
      .withColumn("visit_id",row_number() over windowValue)
        .select($"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"first_form_entered_date",$"visit_date",datediff($"first_form_entered_date",$"visit_date").as("tde"))

    visitSubjectDataDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/subject_visit_tde")
  }


  def populateSubjectVisitTdeWeekCum(){
    val prjWkndSchema = StructType(List(StructField("project_id",StringType,true),StructField("start_date",DateType,true),StructField("end_date",DateType,true),StructField("friday_date",DateType,true)))
    val wkndDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/project_weekend_dates",prjWkndSchema)

    val subjectVisitTdeSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("visit_id",IntegerType,true),StructField("first_form_entered_date",StringType,true),StructField("visit_date",StringType,true),StructField("tde",IntegerType,true)))
    val subjectVisitTdeDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/subject_visit_tde",subjectVisitTdeSchema)

    val subjectvisitTdeWeekCumDF = wkndDF.join(subjectVisitTdeDF)
      .where(subjectVisitTdeDF("project_id") === wkndDF("project_id") and subjectVisitTdeDF("project_id").geq(0) and subjectVisitTdeDF("first_form_entered_date").between(wkndDF("start_date"),wkndDF("friday_date")))
      .select(wkndDF("project_id"),wkndDF("friday_date"),subjectVisitTdeDF("site_ref_id"),subjectVisitTdeDF("subject_name"),subjectVisitTdeDF("study_event_oid"),subjectVisitTdeDF("study_event_repeat_key"),subjectVisitTdeDF("visit_id"),subjectVisitTdeDF("tde"))
      .orderBy("project_id,friday_date asc")

    subjectvisitTdeWeekCumDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/subject_visit_tde_week_cum")


    val subjectvisitTdeWeekCurrDF = wkndDF.join(subjectVisitTdeDF)
      .where(subjectVisitTdeDF("project_id") === wkndDF("project_id") and subjectVisitTdeDF("project_id").geq(0) and subjectVisitTdeDF("first_form_entered_date").between(date_add(wkndDF("friday_date"),-7),wkndDF("friday_date")))
      .select(wkndDF("project_id"),wkndDF("friday_date"),subjectVisitTdeDF("site_ref_id"),subjectVisitTdeDF("subject_name"),subjectVisitTdeDF("study_event_oid"),subjectVisitTdeDF("study_event_repeat_key"),subjectVisitTdeDF("visit_id"),subjectVisitTdeDF("tde"))
      .orderBy("project_id,friday_date asc")
    subjectvisitTdeWeekCurrDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/subject_visit_tde_week_current")

  }

  /*end of cdh Tab1 code*/


  /*start of cdh Tab2 code*/


  def cacheActiveDataPoints(){
    val adpDF = spark.sql("select * from "+eedcRaveDatabaseName+".activedatapoints")
    adpDF.createOrReplaceTempView("activedatapointscached")
  }

  def populateExpectedCrfs(){
    //val alsSchema = StructType(List(StructField("project_oid",StringType,true),StructField("folder_oid",StringType,true),StructField("visit",StringType,true),StructField("allow_add",StringType,true),StructField("target_days",StringType,true),StructField("timezero_folder",StringType,true),StructField("form_oid",StringType,true),StructField("description_of_page",StringType,true),StructField("page_requirements",StringType,true),StructField("always_required_if_visit_was_done",StringType,true),StructField("trigger_condition",StringType,true),StructField("unknown",StringType,true),StructField("matrix",StringType,true)))
    //val alsDF = getDataFrame(alsDataLocation,alsSchema)
    val alsDF = spark.sql("select * from "+eedcRaveDatabaseName+".als")

    val alsFilteredDF = alsDF.filter(!nvlUDF($"page_requirements",lit("0")).like("%Matrix not merged%"))

    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")
    val adpSelectDF = activeDataPointsDF.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp","item_oid","item_value")
        .filter($"audit_sub_category_name".isin("Entered","EnteredEmpty","EnteredByDdeAutoReconcile","EnteredEmptyWithChangeCode","EnteredNonConformant","EnteredWithChangeCode"))


    val adpFilteredDF = adpSelectDF.filter(!nvlUDF($"audit_user_oid",lit("0")).rlike("systemuser|RWS"))
        .select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key").distinct()
    val alsAdpFinalDF = alsFilteredDF.join(adpFilteredDF,adpFilteredDF("study_id") === alsFilteredDF("project_oid") and adpFilteredDF("study_event_oid") === alsFilteredDF("folder_oid"),"inner")
      .select(alsFilteredDF("project_oid"),adpFilteredDF("project_id"),adpFilteredDF("site_ref_id"),adpFilteredDF("subject_name"),adpFilteredDF("study_event_oid"),adpFilteredDF("study_event_repeat_key"),alsFilteredDF("form_oid"))
    alsAdpFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/expected_crfs")

  }


  def populateCrfExpectedSubjects(){
    val exptCrfsSchema = StructType(List(StructField("project_oid",StringType,true),StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true)))
    val exptCfrsDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/expected_crfs",exptCrfsSchema)

    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")

    // start of code to populate rave_subject_current_status
    val rankWindow = Window.partitionBy("project_id","site_ref_id","subject_name").orderBy($"source_id".cast(IntegerType) desc)
    val adpForRaveSubjDF = activeDataPointsDF.filter(length(nvlUDF($"subject_status",lit("0"))).gt("0")).select("project_id","site_ref_id","subject_name","subject_status","source_id")
      .withColumn("rank",rank() over rankWindow)

    val adpForRaveSubjFinalDF = adpForRaveSubjDF.filter($"rank" === 1).select("project_id","site_ref_id","subject_name","subject_status").distinct()

    adpForRaveSubjFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/rave_subject_current_status")
    // end of code to populate rave_subject_current_status

    val adpDF = activeDataPointsDF.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp","item_oid","item_value")
      .filter($"audit_sub_category_name".isin("Entered","EnteredEmpty","EnteredByDdeAutoReconcile","EnteredEmptyWithChangeCode","EnteredNonConformant","EnteredWithChangeCode"))

    val adpFDF = adpDF.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp").withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid")).distinct()
    val alsSelectDF = exptCfrsDF.select(concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid").as("key2"))
    val adpExptCrfsDF = adpFDF.join(alsSelectDF,alsSelectDF("key2") === adpFDF("key1") ,"inner")
        .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key").distinct()
    val crfExptSubjectsDF = adpExptCrfsDF.groupBy("project_id","site_ref_id","subject_name")
        .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRF_Expected_count")
       .select("project_id","site_ref_id","subject_name","CRF_Expected_count")
    crfExptSubjectsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_expected_subject")

  }

  def populateCrfStartedSubjects(){
    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")
    val adpDF = activeDataPointsDF.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp","item_oid","item_value")
      .filter($"audit_sub_category_name".isin("Entered","EnteredEmpty","EnteredByDdeAutoReconcile","EnteredEmptyWithChangeCode","EnteredNonConformant","EnteredWithChangeCode"))


    val adpCFDF = adpDF.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp")
      .filter(!nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key").distinct()
    val adpFinalDF = adpCFDF.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRF_Started_count")
      .select("project_id","site_ref_id","subject_name","CRF_Started_count")
    adpFinalDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_started_subject")

  }

  def populateCrfEntryComplete(){
    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")
    val adpCDF = activeDataPointsDF.filter($"audit_sub_category_name".isin("Entered","EnteredEmpty","EnteredByDdeAutoReconcile","EnteredEmptyWithChangeCode","EnteredNonConformant","EnteredWithChangeCode"))
      .select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp","item_oid","item_value")
      .distinct()

    val rvalsFieldsRequiredSchema = StructType(List(StructField("project_oid",StringType,true),StructField("fields_oid",StringType,true),StructField("fields_sourcedocument",StringType,true),StructField("form_oid",StringType,true),StructField("metadata_version_oid",StringType,true)))
    val rvalsFrDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/rvals_fields_required",rvalsFieldsRequiredSchema)
    val rvalsFrFinalDF = rvalsFrDF.select("project_oid","fields_oid","form_oid","fields_sourcedocument").distinct()
    val apdRvalsFrDF = adpCDF.join(rvalsFrFinalDF,rvalsFrFinalDF("project_oid") === adpCDF("study_id") and rvalsFrFinalDF("form_oid") === adpCDF("form_oid") and rvalsFrFinalDF("fields_oid") === adpCDF("item_oid"),"inner")
      .select(adpCDF("project_id"),adpCDF("study_id"),adpCDF("site_ref_id"),adpCDF("subject_name"),adpCDF("study_event_oid"),adpCDF("study_event_repeat_key"),adpCDF("form_oid"),adpCDF("form_repeat_key"),adpCDF("item_oid"),adpCDF("item_value"))
      .distinct()

    // saving into crf_entry_complete_all
    val adpRvalsDF = apdRvalsFrDF.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key").distinct()
    adpRvalsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_all")

    val crfEntryCompleteDF = apdRvalsFrDF.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "crf_entry_complete")
      .select("project_id","site_ref_id","subject_name","crf_entry_complete")

    // saving into crf_entry_complete
    crfEntryCompleteDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete")
  }


  def populateCrfEntryCompleteBase(){
    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val adpFullDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val adpFullDF = spark.sql("select * from activedatapointscached")

    val crfEntryCompleteAllSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true)))
    val crfEntryCompleteAllDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_all",crfEntryCompleteAllSchema)

    val adpDF = adpFullDF.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_sub_category_name","audit_date_time_stamp")
      .withColumn("key",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key"))
    val crfEntryDF = crfEntryCompleteAllDF.select(concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key").as("key"))
      .distinct()
    val crfEntryCompleteBaseDF = adpDF.alias("adp").join(crfEntryDF.alias("crfe"),$"crfe.key" === $"adp.key","inner")
      .select($"adp.project_id",$"adp.study_id",$"adp.site_ref_id",$"adp.subject_name",$"adp.study_event_oid",$"adp.study_event_repeat_key",$"adp.form_oid",$"adp.form_repeat_key",$"adp.record_id",$"adp.item_oid",$"adp.query_repeat_key",$"adp.audit_sub_category_name",$"adp.audit_date_time_stamp")
    crfEntryCompleteBaseDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_base")

  }


  def populateCrfClosedQueriesSubj(){
    val crfEntryCompleteBaseSchema  = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true)))
    val crfEntryCompleteBaseDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_base",crfEntryCompleteBaseSchema)

    val crfEntryCompleteBase = crfEntryCompleteBaseDF.filter(!nvlUDF($"audit_sub_category_name",lit("0")).isin("QueryClose"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key")
      .distinct()

    val crfClosedQueriesSubjAllDF = crfEntryCompleteBase.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key")
    crfClosedQueriesSubjAllDF.coalesce(10).write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_closed_queries_subject_all")

    val crfClosedQueriesSubjDF = crfEntryCompleteBase.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRF_Closed_Queries_subject")
      .select("project_id","site_ref_id","subject_name","CRF_Closed_Queries_subject")
    crfClosedQueriesSubjDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_closed_queries_subject")

  }

  def populateCrfSdvCompleteSubjTwo(){
    val crfEntryCompleteBaseSchema  = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true)))
    val crfEntryCompleteBaseDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_base",crfEntryCompleteBaseSchema)


    val rankWindow = Window.partitionBy("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid")
      .orderBy(to_date($"audit_date_time_stamp").cast(DateType) desc)
    val crfEntryCompleteBase = crfEntryCompleteBaseDF.filter($"audit_sub_category_name".isin("Verify","UnVerify"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid","audit_sub_category_name","audit_date_time_stamp")
      .withColumn("rank", rank() over rankWindow)
      .filter($"rank" === 1 and $"audit_sub_category_name" === "Verify")
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid","audit_sub_category_name")
      .distinct()

    // populate crf sdv complete subject all

    val crfSdvCompleteSubjAllDF = crfEntryCompleteBase.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key")
      .distinct()
    crfSdvCompleteSubjAllDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_sdv_complete_subject_all")


    val crfSdvCompleteSubjDF = crfEntryCompleteBase.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRF_SDV_Complete_subject")
      .select("project_id","site_ref_id","subject_name","CRF_SDV_Complete_subject")
    crfSdvCompleteSubjDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_sdv_complete_subject_2")

  }

  def populateCrfReadySignBase(){
    val crfEntryCompleteBaseSchema  = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true)))
    val crfEntryCompleteBaseDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_base",crfEntryCompleteBaseSchema)

    val crfSdvCompleteAllSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true)))
    val crfSdvCompleteSubjAllDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_sdv_complete_subject_all",crfSdvCompleteAllSchema)

    val crfClosedQueriesAllSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true)))
    val crfClosedQueriesAllDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_closed_queries_subject_all",crfClosedQueriesAllSchema)

    val crfClosedQueriesAll = crfClosedQueriesAllDF.select(concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key").as("key1"))
    val crfSdvCompleteSubjAll = crfSdvCompleteSubjAllDF.select(concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key").as("key1"))

    val crfEntryCompleteBase = crfEntryCompleteBaseDF.select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","record_id","item_oid","query_repeat_key","audit_sub_category_name","audit_date_time_stamp")
      .withColumn("key1",concat_ws("|",$"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key"))
    val crfReadySignBaseDF = crfEntryCompleteBase.join(crfClosedQueriesAll,crfClosedQueriesAll("key1") === crfEntryCompleteBase("key1"),"inner")
      .select(crfEntryCompleteBase("project_id"),crfEntryCompleteBase("study_id"),crfEntryCompleteBase("site_ref_id"),crfEntryCompleteBase("subject_name"),crfEntryCompleteBase("study_event_oid"),crfEntryCompleteBase("study_event_repeat_key"),crfEntryCompleteBase("form_oid"),crfEntryCompleteBase("form_repeat_key"),crfEntryCompleteBase("record_id"),crfEntryCompleteBase("item_oid"),crfEntryCompleteBase("query_repeat_key"),crfEntryCompleteBase("audit_sub_category_name"),crfEntryCompleteBase("audit_date_time_stamp"),crfEntryCompleteBase("key1"))

    val crfReadySignBaseFDF = crfReadySignBaseDF.join(crfSdvCompleteSubjAll,crfSdvCompleteSubjAll("key1") === crfReadySignBaseDF("key1"),"inner")
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_sub_category_name")

    crfReadySignBaseFDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_ready_for_signature_base")

  }

  def populateCrfReadySignSubjects(){
    val crfReadySignBaseSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true)))
    val crfReadySignBaseDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_ready_for_signature_base",crfReadySignBaseSchema)

    val crfReadySignBase = crfReadySignBaseDF.filter(!nvlUDF($"audit_sub_category_name",lit("0")).isin("ValidESigCredential") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key").distinct()
    val crfReadySignSubjectsDF = crfReadySignBase.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRF_ready_for_signature")
      .select("project_id","site_ref_id","subject_name","CRF_ready_for_signature")
    crfReadySignSubjectsDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_ready_for_signature_subjects")
  }

  def populateCrfPiSignedSubj(){
    val crfEntryCompleteBaseSchema  = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true)))
    val crfEntryCompleteBaseDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_base",crfEntryCompleteBaseSchema)


    val crfEntryBaseDF = crfEntryCompleteBaseDF.filter($"audit_sub_category_name".isin ("ValidESigCredential") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      //.select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","audit_date_time_stamp").distinct()
      .select("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key").distinct()

    val crfPiSignedSubjDF = crfEntryBaseDF.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRFs_PI_Signed_subjects")
      .select("project_id","site_ref_id","subject_name","CRFs_PI_Signed_subjects")
    crfPiSignedSubjDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crfs_pi_signed_subjects")

  }


  def populateActiveEnteredReqSdvDpTabTwo(){
    val crfEntryCompleteBaseSchema  = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true)))
    val crfEntryCompleteBaseDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete_base",crfEntryCompleteBaseSchema)

    val crfEntryBaseDF = crfEntryCompleteBaseDF.filter($"audit_sub_category_name".isin ("Entered","EnteredEmpty","EnteredByDdeAutoReconcile","EnteredEmptyWithChangeCode","EnteredNonConformant","EnteredWithChangeCode","Verify"))
      .select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid").distinct()

    val rvalsFieldsRequiredSchema = StructType(List(StructField("project_oid",StringType,true),StructField("fields_oid",StringType,true),StructField("fields_sourcedocument",StringType,true),StructField("form_oid",StringType,true),StructField("metadata_version_oid",StringType,true)))
    val rvalsFrqDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/rvals_fields_required",rvalsFieldsRequiredSchema)
    val rvalsFrDF = rvalsFrqDF.filter($"fields_sourcedocument" === "Yes").select("project_oid","fields_oid","form_oid","fields_sourcedocument").distinct()


    val activeEnteredSdvDpTabTwoDF = crfEntryBaseDF.join(rvalsFrDF,rvalsFrDF("project_oid") === crfEntryBaseDF("study_id") and rvalsFrDF("form_oid") === crfEntryBaseDF("form_oid") and rvalsFrDF("fields_oid") === crfEntryBaseDF("item_oid"),"inner")
      .select(crfEntryBaseDF("project_id"),crfEntryBaseDF("study_id"),crfEntryBaseDF("site_ref_id"),crfEntryBaseDF("subject_name"),crfEntryBaseDF("study_event_oid"),crfEntryBaseDF("study_event_repeat_key"),crfEntryBaseDF("form_oid"),crfEntryBaseDF("form_repeat_key"),crfEntryBaseDF("item_oid"),rvalsFrDF("fields_sourcedocument"))
      .select("project_id","study_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key","item_oid").distinct()
    activeEnteredSdvDpTabTwoDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/active_entered_required_sdv_datapoints_tab2")

  }


  def populateCrfReqSdvSubj(){
    val activeEnteredSdvDpTabTwoSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("item_oid",StringType,true)))
    val activeEnteredSdvDpTabTwoDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/active_entered_required_sdv_datapoints_tab2",activeEnteredSdvDpTabTwoSchema)

    val activeEnteredDF = activeEnteredSdvDpTabTwoDF.groupBy("project_id","site_ref_id","subject_name")
      .agg(countDistinct("project_id","site_ref_id","subject_name","study_event_oid","study_event_repeat_key","form_oid","form_repeat_key") as "CRF_Required_SDV_subjects")
      .select("project_id","site_ref_id","subject_name","CRF_Required_SDV_subjects")
    activeEnteredDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_required_sdv_subjects")
  }

  def populateAuditQueriesTabTwo(){
    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")

    val adpDF = activeDataPointsDF.select("project_id","site_ref_id","subject_name").distinct()

    val totalOpenQueriesSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("open_query",IntegerType,true)))
    val totalOpenQueriesDF = spark.read.format("orc").schema(totalOpenQueriesSchema).load(warehouseLocation+"/"+edcAggDatabaseName+".db/total_open_queries_subject")

    val queryAuditActionSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("rank",IntegerType,true)))
    val queryAuditActionDF = spark.read.format("orc").schema(queryAuditActionSchema).load(warehouseLocation+"/"+eedcRaveDatabaseName+".db/queryauditactions")

    val totalQueriesOpenAllDF = queryAuditActionDF.filter($"audit_sub_category_name".isin("QueryOpen") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "Total_Open_queries_all")
      .select("project_id","site_ref_id","subject_name","Total_Open_queries_all")

    val totalQueriesClosedDF = queryAuditActionDF.filter($"audit_sub_category_name".isin("QueryClose") and $"rank" === "1" and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "Total_Closed_queries")
      .select("project_id","site_ref_id","subject_name","Total_Closed_queries")

    val totalQueriesAnswdDF = queryAuditActionDF.filter($"audit_sub_category_name".isin("QueryAnswer","QueryAnswerByChange") and $"rank" === "1" and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "Total_Answered_queries")
      .select("project_id","site_ref_id","subject_name","Total_Answered_queries")

    val totalQueriesCancelDF = queryAuditActionDF.filter($"audit_sub_category_name".isin("QueryCancel") and $"rank" === "1" and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "Total_Canceled_queries")
      .select("project_id","site_ref_id","subject_name","Total_Canceled_queries")

    val totalQueriesCancelAllDF = queryAuditActionDF.filter($"audit_sub_category_name".isin("QueryCancel") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "Total_Canceled_queries_all")
      .select("project_id","site_ref_id","subject_name","Total_Canceled_queries_all")

    val adpQueryOpenDF = activeDataPointsDF.filter($"audit_sub_category_name".isin("QueryOpen") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .select($"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid",$"query_repeat_key",to_date($"audit_date_time_stamp").as("open_date"),$"audit_date_time_stamp")

    val adpQueryAnswerDF = activeDataPointsDF.filter($"audit_sub_category_name".isin("QueryAnswer") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .select($"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid",$"query_repeat_key",to_date($"audit_date_time_stamp").as("answer_date"),$"audit_date_time_stamp")

    val adpQueryCloseDF = activeDataPointsDF.filter($"audit_sub_category_name".isin("QueryClose") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .select($"project_id",$"site_ref_id",$"subject_name",$"study_event_oid",$"study_event_repeat_key",$"form_oid",$"form_repeat_key",$"record_id",$"item_oid",$"query_repeat_key",to_date($"audit_date_time_stamp").as("close_date"),$"audit_date_time_stamp")

    val adpQueryEnteredDF = activeDataPointsDF.filter($"audit_sub_category_name".isin("Entered") and !nvlUDF($"form_oid",lit("0")).like("CTMS%") and nvlUDF($"subject_name",lit("0")) =!= "New Subject" and !nvlUDF($"form_oid",lit("0")).like("INTEG%"))
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "Total_entered_datapoints")
      .select($"project_id",$"site_ref_id",$"subject_name",$"Total_entered_datapoints")

    val initialResponseDF = adpQueryOpenDF.alias("open").join(adpQueryAnswerDF.alias("answer"), $"answer.project_id" === $"open.project_id" and $"answer.site_ref_id" === $"open.site_ref_id" and $"answer.subject_name" === $"open.subject_name" and $"answer.study_event_oid" === $"open.study_event_oid" and $"answer.study_event_repeat_key" === $"open.study_event_repeat_key" and $"answer.form_oid" === $"open.form_oid" and $"answer.form_repeat_key" === $"open.form_repeat_key" and $"answer.record_id" === $"open.record_id" and $"answer.item_oid" === $"open.item_oid" and $"answer.query_repeat_key" === $"open.query_repeat_key")
      .select($"open.project_id",$"open.site_ref_id",$"open.subject_name",$"open.study_event_oid",$"open.study_event_repeat_key",$"open.form_oid",$"open.form_repeat_key",$"open.record_id",$"open.item_oid",$"open.query_repeat_key",$"open.audit_date_time_stamp",$"open.open_date",$"answer.answer_date",datediff($"answer.answer_date",$"open.open_date").as("initial_response"))
      .distinct()
      .groupBy(adpQueryOpenDF("project_id"),adpQueryOpenDF("site_ref_id"),adpQueryOpenDF("subject_name"))
      .agg(avg($"initial_response") as "Query_Time_to_Initial_Response")
      .select("project_id","site_ref_id","subject_name","Query_Time_to_Initial_Response")


    val answerToClosureDF = adpQueryAnswerDF.alias("answer").join(adpQueryCloseDF.alias("close"),$"answer.project_id" === $"close.project_id" and $"answer.site_ref_id" === $"close.site_ref_id" and $"answer.subject_name" === $"close.subject_name" and $"answer.study_event_oid" === $"close.study_event_oid" and $"answer.study_event_repeat_key" === $"close.study_event_repeat_key" and $"answer.form_oid" === $"close.form_oid" and $"answer.form_repeat_key" === $"close.form_repeat_key" and $"answer.record_id" === $"close.record_id" and $"answer.item_oid" === $"close.item_oid" and $"answer.query_repeat_key" === $"close.query_repeat_key","inner")
      .select($"answer.project_id",$"answer.site_ref_id",$"answer.subject_name",$"answer.study_event_oid",$"answer.study_event_repeat_key",$"answer.form_oid",$"answer.form_repeat_key",$"answer.record_id",$"answer.item_oid",$"answer.query_repeat_key",$"answer.audit_date_time_stamp",$"answer.answer_date",$"close.close_date",datediff($"close.close_date",$"answer.answer_date").as("answer_to_closer"))
      .distinct()
      .groupBy(adpQueryAnswerDF("project_id"),adpQueryAnswerDF("site_ref_id"),adpQueryAnswerDF("subject_name"))
      .agg(avg($"answer_to_closer") as "Query_Time_Answered_to_Closure")
      .select("project_id","site_ref_id","subject_name","Query_Time_Answered_to_Closure")

    val auditQueriesTabTwoStgDF = adpDF.alias("adp").join(totalOpenQueriesDF.alias("open"),$"open.project_id" === $"adp.project_id" and $"open.site_ref_id" === $"adp.site_ref_id" and $"open.subject_name" === $"adp.subject_name","left")
      .join(totalQueriesOpenAllDF.alias("openall"),$"openall.project_id" === $"adp.project_id" and $"openall.site_ref_id" === $"adp.site_ref_id" and $"openall.subject_name" === $"adp.subject_name","left")
      .join(totalQueriesClosedDF.alias("closed"),$"closed.project_id" === $"adp.project_id" and $"closed.site_ref_id" === $"adp.site_ref_id" and $"closed.subject_name" === $"adp.subject_name","left")
      .join(totalQueriesAnswdDF.alias("answd"),$"answd.project_id" === $"adp.project_id" and $"answd.site_ref_id" === $"adp.site_ref_id" and $"answd.subject_name" === $"adp.subject_name","left")
      .join(totalQueriesCancelDF.alias("cancel"),$"cancel.project_id" === $"adp.project_id" and $"cancel.site_ref_id" === $"adp.site_ref_id" and $"cancel.subject_name" === $"adp.subject_name","left")
      .join(totalQueriesCancelAllDF.alias("cancelall"),$"cancelall.project_id" === $"adp.project_id" and $"cancelall.site_ref_id" === $"adp.site_ref_id" and $"cancelall.subject_name" === $"adp.subject_name","left")
      .select($"adp.project_id",$"adp.site_ref_id",$"adp.subject_name",nvlUDF($"open.open_query",lit("0")).as("Total_Open_queries"),nvlUDF($"closed.Total_Closed_queries",lit("0")).as("Total_Closed_queries"),nvlUDF($"answd.Total_Answered_queries",lit("0")).as("Total_Answered_queries"),nvlUDF($"cancel.Total_Canceled_queries",lit("0")).as("Total_Canceled_queries"),(nvlUDF($"open.open_query",lit("0"))+nvlUDF($"answd.Total_Answered_queries",lit("0"))).as("Total_Unresolved_queries"),(nvlUDF($"open.open_query",lit("0")) + nvlUDF($"closed.Total_Closed_queries",lit("0"))+nvlUDF($"answd.Total_Answered_queries",lit("0"))).as("total_queries"),nvlUDF($"openall.Total_Open_queries_all",lit("0")).as("Total_Open_queries_all"),nvlUDF($"cancelall.Total_Canceled_queries_all",lit("0")).as("Total_Canceled_queries_all"))


    val auditQueriesTabTwoDF = auditQueriesTabTwoStgDF.alias("stg").join(initialResponseDF.alias("initial"),$"initial.project_id" === $"stg.project_id" and $"initial.site_ref_id" === $"stg.site_ref_id" and $"initial.subject_name" === $"stg.subject_name","left")
      .join(answerToClosureDF.alias("answerC"),$"answerC.project_id" === $"stg.project_id" and $"answerC.site_ref_id" === $"stg.site_ref_id" and $"answerC.subject_name" === $"stg.subject_name","left")
      .join(adpQueryEnteredDF.alias("entered"),$"entered.project_id" === $"stg.project_id" and $"entered.site_ref_id" === $"stg.site_ref_id" and $"entered.subject_name" === $"stg.subject_name","left")
      .select($"stg.project_id",$"stg.site_ref_id",$"stg.subject_name",$"stg.Total_Open_queries".cast(IntegerType),$"stg.Total_Closed_queries".cast(IntegerType),$"stg.Total_Answered_queries".cast(IntegerType),$"stg.Total_Canceled_queries".cast(IntegerType),$"stg.Total_Unresolved_queries".cast(IntegerType),$"stg.total_queries".cast(IntegerType),nvlUDF($"initial.Query_Time_to_Initial_Response",lit("0")).cast(DoubleType).as("Query_Time_to_Initial_Response"),nvlUDF($"answerC.Query_Time_Answered_to_Closure",lit("0")).cast(DoubleType).as("Query_Time_Answered_to_Closure"),(($"stg.Total_Open_queries_all" - $"stg.Total_Canceled_queries_all")/nvlUDF($"entered.Total_entered_datapoints",lit("0"))*100).cast(DoubleType).as("Data_Query_Ratio"),(nvlUDF($"stg.Total_Open_queries_all",lit("0")) - nvlUDF($"stg.Total_Canceled_queries_all",lit("0"))).cast(IntegerType).as("data_query_ratio_nume"),nvlUDF($"entered.Total_entered_datapoints",lit("0")).cast(IntegerType).as("data_query_ratio_deno"))

    auditQueriesTabTwoDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/audit_queries_tab2")

  }

  def populateAuditCrfsTabTwo(){
    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")

    val crfExpectedSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_expected_count",IntegerType,true)))
    val crfExpectedSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_expected_subject",crfExpectedSubjSchema)

    val crfEntryCompleteSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_entry_complete",IntegerType,true)))
    val crfEntryCompleteDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_entry_complete",crfEntryCompleteSchema)

    val crfSdvCompleteSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_sdv_complete_subject",IntegerType,true)))
    val crfSdvCompleteSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_sdv_complete_subject_2",crfSdvCompleteSubjSchema)

    val crfClosedQueriesSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_closed_queries_subject",IntegerType,true)))
    val crfClosedQueriesSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_closed_queries_subject",crfClosedQueriesSubjSchema)

    val crfReadySignSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_ready_for_signature",IntegerType,true)))
    val crfReadySignSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_ready_for_signature_subjects",crfReadySignSubjSchema)

    val crfPiSignSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crfs_pi_signed_subjects",IntegerType,true)))
    val crfPiSignSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crfs_pi_signed_subjects",crfPiSignSubjSchema)

    val crfOpenedQueriesSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_opened_queries_subject",IntegerType,true)))
    val crfOpenedQueriesSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_opened_queries_subject",crfOpenedQueriesSubjSchema)

    val crfReqSdvSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_required_sdv_subjects",IntegerType,true)))
    val crfReqSdvSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_required_sdv_subjects",crfReqSdvSubjSchema)

    val crfStartedSubjSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_started_count",IntegerType,true)))
    val crfStartedSubjDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/crf_started_subject",crfStartedSubjSchema)


    val auditCrfsTabTwoDF = activeDataPointsDF.alias("adp").join(crfExpectedSubjDF.alias("crfes"), $"crfes.project_id" === $"adp.project_id" and $"crfes.site_ref_id" === $"adp.site_ref_id" and $"crfes.subject_name" === $"adp.subject_name","left")
      .join(crfEntryCompleteDF.alias("crfec"),$"crfec.project_id" === $"adp.project_id" and $"crfec.site_ref_id" === $"adp.site_ref_id" and $"crfec.subject_name" === $"adp.subject_name","left")
      .join(crfSdvCompleteSubjDF.alias("crfscs"),$"crfscs.project_id" === $"adp.project_id" and $"crfscs.site_ref_id" === $"adp.site_ref_id" and $"crfscs.subject_name" === $"adp.subject_name","left")
      .join(crfClosedQueriesSubjDF.alias("crfcqs"),$"crfcqs.project_id" === $"adp.project_id" and $"crfcqs.site_ref_id" === $"adp.site_ref_id" and $"crfcqs.subject_name" === $"adp.subject_name","left")
      .join(crfReadySignSubjDF.alias("crfrss"),$"crfrss.project_id" === $"adp.project_id" and $"crfrss.site_ref_id" === $"adp.site_ref_id" and $"crfrss.subject_name" === $"adp.subject_name","left")
      .join(crfPiSignSubjDF.alias("crfpss"),$"crfpss.project_id" === $"adp.project_id" and $"crfpss.site_ref_id" === $"adp.site_ref_id" and $"crfpss.subject_name" === $"adp.subject_name","left")
      .join(crfOpenedQueriesSubjDF.alias("crfoqs"),$"crfoqs.project_id" === $"adp.project_id" and $"crfoqs.site_ref_id" === $"adp.site_ref_id" and $"crfoqs.subject_name" === $"adp.subject_name","left")
      .join(crfReqSdvSubjDF.alias("crfress"),$"crfress.project_id" === $"adp.project_id" and $"crfress.site_ref_id" === $"adp.site_ref_id" and $"crfress.subject_name" === $"adp.subject_name","left")
      .join(crfStartedSubjDF.alias("crfss"),$"crfss.project_id" === $"adp.project_id" and $"crfss.site_ref_id" === $"adp.site_ref_id" and $"crfss.subject_name" === $"adp.subject_name","left")
      .select($"adp.project_id",$"adp.site_ref_id",$"adp.subject_name",$"crfes.CRF_Expected_count".cast(IntegerType),$"crfec.crf_entry_complete".cast(IntegerType),$"crfscs.CRF_SDV_Complete_subject".cast(IntegerType),$"crfcqs.CRF_Closed_Queries_subject".cast(IntegerType),$"crfrss.crf_ready_for_signature".cast(IntegerType),$"crfpss.CRFs_PI_Signed_subjects".cast(IntegerType),$"crfoqs.CRF_Opened_Queries_subject".cast(IntegerType),$"crfress.CRF_Required_SDV_subjects".cast(IntegerType),(nvlUDF($"crfec.crf_entry_complete",lit("0")) + nvlUDF($"crfscs.CRF_SDV_Complete_subject",lit("0")) + nvlUDF($"crfcqs.CRF_Closed_Queries_subject",lit("0")) + nvlUDF($"crfrss.crf_ready_for_signature",lit("0")) + nvlUDF($"crfpss.CRFs_PI_Signed_subjects",lit("0"))).cast(IntegerType).as("PI_Signature_num"),((nvlUDF($"crfes.CRF_Expected_count",lit("0")) * 4) + nvlUDF($"crfress.CRF_Required_SDV_subjects",lit("0"))).cast(IntegerType).as("PI_Signature_den"),$"crfss.CRF_Started_count".cast(IntegerType))
      .distinct()

    auditCrfsTabTwoDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/audit_crfs_tab2")

}

  def populateAuditQueriesCrfsTermsTabTwo(){
    val auditCrfsTabTwoSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("crf_expected_count",IntegerType,true),StructField("crf_entry_complete",IntegerType,true),StructField("crf_sdv_complete_subject",IntegerType,true),StructField("crf_closed_queries_subject",IntegerType,true),StructField("crf_ready_for_signature",IntegerType,true),StructField("crfs_pi_signed_subjects",IntegerType,true),StructField("crf_opened_queries_subject",IntegerType,true),StructField("crf_required_sdv_subjects",IntegerType,true),StructField("pi_signature_num",IntegerType,true),StructField("pi_signature_den",IntegerType,true),StructField("crf_started_count",IntegerType,true)))
    val auditCrfsTabTwoDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/audit_crfs_tab2",auditCrfsTabTwoSchema)

    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")


    val adpDF = activeDataPointsDF.select("project_id","site_ref_id","subject_name").distinct()

    val auditQueriesTabTwoSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("total_open_queries",IntegerType,true),StructField("total_closed_queries",IntegerType,true),StructField("total_answered_queries",IntegerType,true),StructField("total_canceled_queries",IntegerType,true),StructField("total_unresolved_queries",IntegerType,true),StructField("total_queries",IntegerType,true),StructField("query_time_to_initial_response",DoubleType,true),StructField("query_time_answered_to_closure",DoubleType,true),StructField("data_query_ratio",DoubleType,true),StructField("data_query_ratio_nume",IntegerType,true),StructField("data_query_ratio_deno",IntegerType,true)))
    val auditQueriesTabTwoDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/audit_queries_tab2",auditQueriesTabTwoSchema)

    val auditQueriesCrfsTermsTabTwoDF = adpDF.join(auditCrfsTabTwoDF,auditCrfsTabTwoDF("project_id") === adpDF("project_id") and auditCrfsTabTwoDF("site_ref_id") === adpDF("site_ref_id") and auditCrfsTabTwoDF("subject_name") === adpDF("subject_name"),"left")
      .join(auditQueriesTabTwoDF,auditQueriesTabTwoDF("project_id") === adpDF("project_id") and auditQueriesTabTwoDF("site_ref_id") === adpDF("site_ref_id") and auditQueriesTabTwoDF("subject_name") === adpDF("subject_name"),"left")
      .select(adpDF("project_id"),adpDF("site_ref_id"),adpDF("subject_name"),lit("").as("budgeted_crfs"),auditCrfsTabTwoDF("CRF_Expected_count").cast(IntegerType).as("expected_crf"),auditCrfsTabTwoDF("CRF_Started_count").cast(IntegerType).as("crf_started"),auditCrfsTabTwoDF("CRF_SDV_Complete_subject").cast(IntegerType).as("sdv_complete"),auditCrfsTabTwoDF("CRF_Required_SDV_subjects").cast(IntegerType).as("crf_require_sdv"),auditCrfsTabTwoDF("crf_entry_complete").cast(IntegerType).as("completed_crfs"),auditCrfsTabTwoDF("CRFs_PI_Signed_subjects").cast(IntegerType).as("crf_signature_complete"),auditCrfsTabTwoDF("crf_entry_complete").cast(IntegerType).as("crfs_completed"),auditCrfsTabTwoDF("CRF_ready_for_signature").cast(IntegerType),auditCrfsTabTwoDF("CRF_Closed_Queries_subject").cast(IntegerType).as("CRF_QueryClosed"),auditCrfsTabTwoDF("CRF_Opened_Queries_subject").cast(IntegerType).as("CRF_Queryopen"),auditCrfsTabTwoDF("PI_Signature_num").cast(IntegerType).as("prog_to_pi_signature_nume"),auditCrfsTabTwoDF("PI_Signature_den").cast(IntegerType).as("prog_to_pi_signature_deno"),auditQueriesTabTwoDF("data_query_ratio_nume").cast(IntegerType),auditQueriesTabTwoDF("data_query_ratio_deno").cast(IntegerType),auditQueriesTabTwoDF("total_open_queries").cast(IntegerType),auditQueriesTabTwoDF("total_closed_queries").cast(IntegerType),auditQueriesTabTwoDF("total_answered_queries").cast(IntegerType),auditQueriesTabTwoDF("total_unresolved_queries").cast(IntegerType),auditQueriesTabTwoDF("total_queries").cast(IntegerType),auditQueriesTabTwoDF("query_time_to_initial_response").cast(DoubleType),auditQueriesTabTwoDF("query_time_answered_to_closure").cast(DoubleType),auditQueriesTabTwoDF("data_query_ratio").cast(DoubleType))

    auditQueriesCrfsTermsTabTwoDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+edcAggDatabaseName+".db/audit_queries_crfs_terms_tab2")

  }

  def populatePdCDHTabTwo(){
    // start of code for backing up previous run data into pd_cdh2_bak table
    val pdCdh2PrevDF = spark.sql("select * from sbi_presentation.pd_cdh2")
    pdCdh2PrevDF.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+sbiPresDatabaseName+".db/pd_cdh2_bak")
    // end of code for backing up previous run data into pd_cdh2_bak table

    //val activeDataPointsSchema = StructType(List(StructField("project_id",StringType,true),StructField("study_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("study_event_oid",StringType,true),StructField("study_event_repeat_key",StringType,true),StructField("form_oid",StringType,true),StructField("form_repeat_key",StringType,true),StructField("record_id",StringType,true),StructField("item_oid",StringType,true),StructField("item_value",StringType,true),StructField("meta_data_version_oid",StringType,true),StructField("query_repeat_key",StringType,true),StructField("audit_sub_category_name",StringType,true),StructField("audit_date_time_stamp",StringType,true),StructField("source_id",StringType,true),StructField("audit_user_oid",StringType,true),StructField("query_recipient",StringType,true),StructField("subject_status",StringType,true),StructField("query_value",StringType,true),StructField("key1",StringType,true)))
    //val activeDataPointsDF = getDataFrame(warehouseLocation+"/"+eedcRaveDatabaseName+".db/activedatapoints",activeDataPointsSchema)
    val activeDataPointsDF = spark.sql("select * from activedatapointscached")

    val auditQueriesCrfsTermsTabTwoDFSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("budgeted_crfs",StringType,true),StructField("expected_crf",IntegerType,true),StructField("crf_started",IntegerType,true),StructField("sdv_complete",IntegerType,true),StructField("crf_require_sdv",IntegerType,true),StructField("completed_crfs",IntegerType,true),StructField("crf_signature_complete",IntegerType,true),StructField("crfs_completed",IntegerType,true),StructField("crf_ready_for_signature",IntegerType,true),StructField("crf_queryclosed",IntegerType,true),StructField("crf_queryopen",IntegerType,true),StructField("prog_to_pi_signature_nume",IntegerType,true),StructField("prog_to_pi_signature_deno",IntegerType,true),StructField("data_query_ratio_nume",IntegerType,true),StructField("data_query_ratio_deno",IntegerType,true),StructField("total_open_queries",IntegerType,true),StructField("total_closed_queries",IntegerType,true),StructField("total_answered_queries",IntegerType,true),StructField("total_unresolved_queries",IntegerType,true),StructField("total_queries",IntegerType,true),StructField("query_time_to_initial_response",DoubleType,true),StructField("query_time_answered_to_closure",DoubleType,true),StructField("data_query_ratio",DoubleType,true)))
    val auditQueriesCrfsTermsTabTwoDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/audit_queries_crfs_terms_tab2",auditQueriesCrfsTermsTabTwoDFSchema)

    val adpOpenSystemQueries = activeDataPointsDF.filter($"audit_user_oid"  === "systemuser" and $"audit_sub_category_name" === "QueryOpen")
    val adpCancelSystemQueries = activeDataPointsDF.filter( $"audit_sub_category_name".isin("QueryCancel","MigQueryClosed"))
      .select("project_id","query_repeat_key").distinct()

    val systemQueriesDF = adpOpenSystemQueries.alias("opensys").join(adpCancelSystemQueries.alias("cancelsys"),$"cancelsys.query_repeat_key" === $"opensys.query_repeat_key" and $"cancelsys.project_id" === $"opensys.project_id","left")
      .filter($"cancelsys.query_repeat_key".isNull)
      .select($"opensys.project_id",$"opensys.site_ref_id",$"opensys.subject_name")
    .groupBy("project_id","site_ref_id","subject_name")
    .agg(count(lit("*")) as "system_queries")
      .select("project_id","site_ref_id","subject_name","system_queries")

    val adpOpenManualQueries = activeDataPointsDF.filter(nvlUDF($"audit_user_oid",lit("0"))  =!= "systemuser" and $"audit_sub_category_name" === "QueryOpen")


    val manualQueriesDF = adpOpenManualQueries.alias("openmn").join(adpCancelSystemQueries.alias("cancelsys"),$"cancelsys.query_repeat_key" === $"openmn.query_repeat_key" and $"cancelsys.project_id" === $"openmn.project_id","left")
      .filter($"cancelsys.query_repeat_key".isNull)
      .select($"openmn.project_id",$"openmn.site_ref_id",$"openmn.subject_name")
      .groupBy("project_id","site_ref_id","subject_name")
      .agg(count(lit("*")) as "manual_queries")
      .select("project_id","site_ref_id","subject_name","manual_queries")


    val cdmSite = spark.sql("select distinct project_code,site_number,country_code,country_name from cdm.site")

    val raveSubjectCurrStatusSchema = StructType(List(StructField("project_id",StringType,true),StructField("site_ref_id",StringType,true),StructField("subject_name",StringType,true),StructField("subject_status",StringType,true)))
    val raveSubjectCurrStatusDF = getDataFrame(warehouseLocation+"/"+edcAggDatabaseName+".db/rave_subject_current_status",raveSubjectCurrStatusSchema)
    //val raveSubjectCurrStatusDF = spark.sql("select * from edc_rave_aggregation.rave_subject_current_status")

    val pdcdhTabTwo = auditQueriesCrfsTermsTabTwoDF.alias("crfterms").join(raveSubjectCurrStatusDF.alias("raves"),$"raves.project_id" === $"crfterms.project_id" and $"raves.site_ref_id" === $"crfterms.site_ref_id" and $"raves.subject_name" === $"crfterms.subject_name","left")
      .join(systemQueriesDF.alias("sysq"),$"sysq.project_id" === $"crfterms.project_id" and $"sysq.site_ref_id" === $"crfterms.site_ref_id" and $"sysq.subject_name" === $"crfterms.subject_name","left")
      .join(manualQueriesDF.alias("mnq"),$"mnq.project_id" === $"crfterms.project_id" and $"mnq.site_ref_id" === $"crfterms.site_ref_id" and $"mnq.subject_name" === $"crfterms.subject_name","left")
      .join(cdmSite.alias("cdms"),$"cdms.site_number" === $"crfterms.site_ref_id" and $"cdms.project_code" === $"crfterms.project_id","left")
      .select($"crfterms.project_id",$"crfterms.site_ref_id",$"crfterms.subject_name",$"raves.subject_status",$"crfterms.budgeted_crfs".cast(DoubleType),$"crfterms.expected_crf".cast(DoubleType),$"crfterms.crf_started".cast(DoubleType),$"crfterms.sdv_complete".cast(DoubleType),$"crfterms.crf_require_sdv".cast(DoubleType),$"crfterms.completed_crfs".cast(DoubleType),$"crfterms.crf_signature_complete".cast(DoubleType),$"crfterms.crfs_completed".cast(DoubleType),$"crfterms.CRF_ready_for_signature".cast(DoubleType),$"crfterms.CRF_Queryopen".cast(DoubleType),$"crfterms.CRF_QueryClosed".cast(DoubleType),$"crfterms.total_open_queries".cast(DoubleType),$"crfterms.total_closed_queries".cast(DoubleType),$"crfterms.total_answered_queries".cast(DoubleType),$"crfterms.total_unresolved_queries".cast(DoubleType),$"crfterms.total_queries".cast(DoubleType),$"crfterms.query_time_to_initial_response".cast(DoubleType),$"crfterms.query_time_answered_to_closure".cast(DoubleType),$"crfterms.data_query_ratio_nume".cast(DoubleType),$"crfterms.data_query_ratio_deno".cast(DoubleType),$"cdms.country_name",$"crfterms.prog_to_pi_signature_nume".cast(DoubleType),$"crfterms.prog_to_pi_signature_deno".cast(DoubleType),$"sysq.system_queries".cast(DoubleType),$"mnq.manual_queries".cast(DoubleType))
      .withColumn("last_processed_date",current_timestamp())


    pdcdhTabTwo.write.mode("overwrite").format("orc").save(warehouseLocation+"/"+sbiPresDatabaseName+".db/pd_cdh2")

  }

  /*end of cdh Tab2 code*/
}
