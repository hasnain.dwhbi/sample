package com.syneos.custom

import java.io.DataOutputStream
import java.net.URL
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import org.apache.spark.sql.functions._

class CdhCommon {

  /*def getCdhRawDataUrl(directory: String,dt: String): String = {
    directory+"/rave_ws_dt=" + dt + "/study_id=*"
  }
  */
  def getCdhRawDataUrl(directory: String,dt: String): String = {
    directory+"/rave_ws_dt=" + dt
  }

  def sendMessageToSlack(message: String): Unit ={
    val url = "https://hooks.slack.com/services/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"
    val obj: URL = new URL(url)
    val urlConn = obj.openConnection
    urlConn.setRequestProperty("Content-Type","application/json")
    urlConn.setRequestProperty("Charset", "UTF-8")
    val urlParameters = s"""{"text":"$message"}"""
    // Send post request
    urlConn.setDoInput(true)
    urlConn.setDoOutput(true)
    val wr: DataOutputStream = new DataOutputStream(urlConn.getOutputStream())
    wr.writeBytes(urlParameters)
    wr.flush()
    wr.close()
    val in = urlConn.getInputStream()
    in.close
  }

  val replaceXmlHeader = udf((xmldata: String) => {
    xmldata.replace("<?xml version=\"1.1\"?>","").replace("\\r","").replace("\\n","")
  })

  val nvlUDF = udf((column_value:String,replace_value: String) => {
    if(column_value == null){
      replace_value
    } else {
      column_value
    }
  })

  val getAuditSubCategoryByFileName = udf((column_name: String, delimiter: String, pos: Integer, pattern: String ) => {
    column_name.split(delimiter)(pos).replace(pattern,"")
  })

}
